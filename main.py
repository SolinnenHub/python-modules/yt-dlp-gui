import sys
import os
import configparser
import threading
import yt_dlp
from PyQt5.QtWidgets import QApplication, QWidget, QVBoxLayout, QHBoxLayout, QLineEdit, QPushButton, QTextEdit, QLabel, QFileDialog, QMessageBox
from PyQt5.QtCore import pyqtSignal, QObject, QThread

CONFIG_FILE = 'settings.ini'

def load_settings():
    config = configparser.ConfigParser()
    if os.path.exists(CONFIG_FILE):
        config.read(CONFIG_FILE)
        return config.get('Settings', 'download_path', fallback=os.getcwd())
    else:
        return os.getcwd()

def save_settings(path):
    config = configparser.ConfigParser()
    config['Settings'] = {'download_path': path}
    with open(CONFIG_FILE, 'w') as configfile:
        config.write(configfile)

class DownloaderWorker(QObject):
    progress = pyqtSignal(str)
    finished = pyqtSignal(str)
    error = pyqtSignal(str)

    def __init__(self, url, download_path):
        super().__init__()
        self.url = url
        self.download_path = download_path

    def run(self):
        ydl_opts = {
            'format': 'bestaudio/best',
            'outtmpl': os.path.join(self.download_path, '%(title)s.%(ext)s'),
            'postprocessors': [{
                'key': 'FFmpegExtractAudio',
                'preferredcodec': 'wav',
                'preferredquality': '192',
            }],
            'progress_hooks': [self.update_progress],
            'quiet': True,
            'no_color': True
        }

        try:
            with yt_dlp.YoutubeDL(ydl_opts) as ydl:
                info = ydl.extract_info(self.url, download=False)
                fname = ydl.prepare_filename(info)
                fname = fname[:fname.rfind(".")]
                fname = fname[fname.rfind("\\")+1:]
                ydl.download([self.url])
            self.finished.emit(f'\n{fname}\n\nDownload completed!\n')
        except Exception as e:
            if "Download canceled" not in str(e):
                self.error.emit(str(e))

    def update_progress(self, d):
        if d['status'] == 'downloading':
            elapsed = d.get('_elapsed_str', 'N/A')
            eta = d.get('_eta_str', 'N/A')
            speed = d.get('_speed_str', 'N/A')
            self.progress.emit(f'Downloading... {elapsed} elapsed, {eta} remaining, speed: {speed}')

class Downloader(QWidget):
    def __init__(self):
        super().__init__()

        self.initUI()

    def initUI(self):
        self.setWindowTitle('YouTube Audio Downloader')

        vbox = QVBoxLayout()

        # URL input
        hbox1 = QHBoxLayout()
        self.url_input = QLineEdit(self)
        self.url_input.setPlaceholderText('URL')
        self.url_input.selectAll()
        hbox1.addWidget(self.url_input)

        self.paste_button = QPushButton('Paste', self)
        self.paste_button.clicked.connect(self.paste_url)
        hbox1.addWidget(self.paste_button)
        vbox.addLayout(hbox1)

        # Download path
        hbox2 = QHBoxLayout()
        self.path_label = QLabel('Download Path:', self)
        hbox2.addWidget(self.path_label)

        self.path_input = QLineEdit(self)
        self.path_input.setText(load_settings())
        self.path_input.setReadOnly(True)
        hbox2.addWidget(self.path_input)

        self.path_button = QPushButton('...', self)
        self.path_button.clicked.connect(self.select_path)
        hbox2.addWidget(self.path_button)
        vbox.addLayout(hbox2)

        # Buttons
        hbox3 = QHBoxLayout()
        self.download_button = QPushButton('Download', self)
        self.download_button.clicked.connect(self.download_video)
        hbox3.addWidget(self.download_button)

        self.exit_button = QPushButton('Exit', self)
        self.exit_button.clicked.connect(self.exit_program)
        hbox3.addWidget(self.exit_button)
        vbox.addLayout(hbox3)

        # Status log
        self.status_log = QTextEdit(self)
        self.status_log.setReadOnly(True)
        vbox.addWidget(self.status_log)

        self.setLayout(vbox)
        self.resize(600, 400)

    def paste_url(self):
        self.url_input.setText(QApplication.clipboard().text())

    def select_path(self):
        path = QFileDialog.getExistingDirectory(self, 'Select Download Directory')
        if path:
            save_settings(path)
            self.path_input.setText(path)

    def download_video(self):
        url = self.url_input.text()
        download_path = self.path_input.text()

        if not url:
            QMessageBox.critical(self, 'Error', 'Enter a URL!')
            return

        self.thread = QThread()
        self.worker = DownloaderWorker(url, download_path)
        self.worker.moveToThread(self.thread)
        self.thread.started.connect(self.worker.run)
        self.worker.progress.connect(self.update_progress)
        self.worker.finished.connect(self.download_finished)
        self.worker.error.connect(self.download_error)
        self.thread.start()

    def update_progress(self, message):
        self.status_log.append(message)

    def download_finished(self, message):
        self.status_log.append(message)
        self.thread.quit()
        self.thread.wait()

    def download_error(self, message):
        QMessageBox.critical(self, 'Error', message)
        self.status_log.append('Error during download\n')
        self.thread.quit()
        self.thread.wait()

    def exit_program(self):
        self.close()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Downloader()
    ex.show()
    sys.exit(app.exec_())
