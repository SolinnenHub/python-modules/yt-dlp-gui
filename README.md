### YouTube Audio Downloader

This Python application provides a graphical interface to download audio in the best quality from YouTube videos. It utilizes `PyQt5` for the GUI and `yt_dlp` (a fork of `youtube-dl`) for downloading videos.

![Application Screenshot](screenshot.png)

#### Features:
- **Download Audio**: Enter a YouTube video URL and download the audio in the best available quality.
- **Customizable Download Path**: Save downloaded audio files to a custom directory specified by the user.
- **Download Progress**: Real-time progress updates during the download process.
- **Persistent Settings**: Save and load download path settings across sessions.

#### Dependencies:
- Python 3.x
- `PyQt5` (Python bindings for the Qt application framework)
- `yt_dlp` (YouTube downloader library)

#### How to Use:

1. Install the dependencies:

```bash
pip install PyQt5 yt_dlp
```

2. Run the application:

```bash
python main.py
```

- The GUI window will appear where you can enter a YouTube URL.
- Select a download path by clicking the '...' button or use the default path.
- Click 'Download' to initiate the download process.
- Progress updates and completion messages will be displayed in the application window.
